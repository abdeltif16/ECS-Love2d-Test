files['main.lua']={global = false, ignore={"212","211"}, allow_defined = true, allow_defined_top = true,}

-- Copied&modified from LIKO-12's .luacheckr
ignore = {
	"212", --Unused argument.
	"213", --Unused loop variable.
	"611", --A line consists of nothing but whitespace.
	"613", --Trailing whitespace in a string.
  "631", --Line too long
}

global = false
